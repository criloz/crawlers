$root = {}
$root.Base = require "./base"
$root.cheerio = require "cheerio"
$root.querystring = require "querystring"
$root.moment = require "moment"

module.exports = class extends $root.Base

  """
      PriceLine scrapper
  """

  name: "PriceLine"
  domain: "priceline.com"
  search_express_deal_url: "http://www.priceline.com/hotelxd/searchDeals.do"
  search_hotel_url: "https://www.priceline.com/smartphone/hotel/hotelsearch.do"

  get_initial_params: (options, callback)->
    """ get cookies plf and jsk and forms params
    return [error], jsk, fls and body
    """

    self = @

    """YYYY-MM-DD ->  MM/DD/YYYY"""
    options.start_date = $root.moment(options.start_date, "YYYY-MM-DD").format("MM/DD/YYYY")
    options.end_date = $root.moment(options.end_date, "YYYY-MM-DD").format("MM/DD/YYYY")



    request_options=
      timeout:options.timeout
      uri:"http://www.priceline.com/hotels/"

    @request request_options, (error, response, body)->

      if error
        return callback error

      if response.statusCode != 200
        return callback new $root.Base.CrawlerHttpError("Unknown error",
          response.statusCode,
          response.headers,
          body,
          request_options)

      try

        """ get jsk and plf params """
        found = body.match /jsk: "([0-9a-z]+)",/

        if not found?
          found =  body.match /jsk=(.+)&/

        [strmatch, jsk] = found

        if not jsk?
          return callback new $root.Base.ParserError("RegExp error triying to obatain the parameter jsk",
            self.name,
            body)

        options.jsk = jsk
        options.plf = 'PCLN'
        options.body = body
        return  callback null, options

      catch error
        new_error =  new $root.Base.ParserError("Error in get_initial_params:",
          self.name,
          body)
        new_error.trace_back = error.stack
        return callback new_error


  translate_on_demand: (options, callback)->

    self = @

    """ get cookies plf, jsk and forms params """
    return self.get_initial_params options, (error, new_options)->

      if error
        return callback error

      query =
        jsk: new_options.jsk
        'function': "type_ahead"
        ss: new_options.location

      request_options=
        qs: query
        timeout:new_options.timeout
        uri:"https://www.priceline.com/smartphone/hotel/citysearch.do"

      self.request request_options, (error, response, body) ->

        if error
          return callback error

        if response.statusCode != 200
          return callback new $root.Base.CrawlerHttpError("Unknown error",
            response.statusCode,
            response.headers,
            body,
            request_options)
        try
          json_body = JSON.parse body

          if not json_body.tips? or  json_body.tips.length == 0
            new_error = new $root.Base.CrawlerHttpError("Bad input city for priceline",
              response.statusCode,
              response.headers,
              body,
              request_options)
            return callback new_error

          new_options.location_translated = json_body.tips[0].pid
          return callback null, new_options

        catch error
          new_error =  new $root.Base.ParserError("Error triying to obtain the city id:", self.name, body)
          new_error.trace_back = error.stack
          return callback new_error

  search_hotel:(ext_options, ext_callback)->


    self = @

    execute_query = (options, callback)->

      options.offset?=0

      query=
        jsk:options.jsk
        cityid:options.location_translated
        checkindate:options.start_date
        checkoutdate:options.end_date
        offset:options.offset
        sort:"popularity"
        lat:0
        lon:0
        minStar:1

      request_options =
        uri: self.search_hotel_url
        qs: query
        timeout: options.timeout
        encoding:"binary"

      self.request request_options, (error, response, body) ->

        if error
          return callback error

        if response.statusCode != 200
          return callback new $root.Base.CrawlerHttpError("Unknown error",
            response.statusCode,
            response.headers,
            body,
            request_options)

        try
          json_body = JSON.parse body

          results = json_body.hotels.map (element)->

            if not element.remainingRooms?
              return null

            if element.remainingRooms < options.rooms
              return null

            query_deep_link =
              jsk: options.jsk
              plf: "PCLH"
              propID: element.pclnHotelID

            result=
              name: element.hotelName
              price: parseFloat(element.merchPrice)
              neighborhood: element.neighborhood
              stars: parseFloat(element.starRating)
              deeplink: "http://www.priceline.com/hotel/hotelOverviewGuide.do?" + $root.querystring.stringify(query_deep_link)

            if element.overallRatingScore != 0
              result.rating = '' + element.overallRatingScore
            result.neighborhood += " area"
            result

          results = results.filter (x) -> x?


          if (json_body.totalListSize - (options.offset + json_body.listSize)) >= 0  and json_body.listSize>=5
            options.offset += json_body.listSize
            #emit a true saying that the script will be continue  crawling to other pages
            callback(null,results, true)
            return self.search_hotel(options,callback)

          else
            #emit signal to finish
            #result have length 0
            return callback(null,results)


        catch error
          new_error =  new $root.Base.ParserError("Error triying to parse the hotel in search_hotel:", self.name, body)
          new_error.trace_back = error.stack
          return callback new_error

    if ext_options.jsk?
      execute_query(ext_options, ext_callback)
    else
      self.get_initial_params ext_options, (error, new_options)->

        if error
          return ext_callback error

        execute_query(new_options, ext_callback)



  search_express_deal:(ext_options, ext_callback)->
    self = @

    execute_query = (options, callback)->
      """ query params """

      query =
        plf:options.plf
        jsk:options.jsk
        src:'RTL_LST_TAB'
        noWait:'Y'
        cityName:options.location
        checkInDate:options.start_date
        checkOutDate:options.end_date
        numberOfRooms:options.rooms

      returnURL = '/hotel/searchHotels.do?' + $root.querystring.stringify(query)
      query.returnURL = returnURL

      request_options =
        uri: self.search_express_deal_url
        qs: query
        timeout: options.timeout

      self.request request_options, (error, response, body)->

        if error
          return callback error

        try

          if response.statusCode != 200
            return callback new $root.Base.CrawlerHttpError("Unknown error",
              response.statusCode,
              response.headers,
              body,
              request_options)

          [definition, deal_data_json] = body.match /var dealData = (.+);/

          if not deal_data_json?
            return callback new $root.Base.ParserError("RegExp error triying to obatain deal data",
              self.name,
              body)

          deal_data = JSON.parse(deal_data_json)
          results = deal_data.deals.map (element)->
            query =
              hdsk:deal_data.hdsk
              dealID:element.id
              dealKey:element.dealKey
              itinKey:deal_data.itinKey
              qdp:element.price
              filter: "pn=1;amen=;area=;price=;star="

            result=
              stars: parseFloat(element.star)
              price: parseFloat(element.price)
              deeplink: deal_data.detailURLBase + '&' + $root.querystring.stringify(query)

            if element.score? and element.score!=""
              result.rating = element.score.toString()

            if element.name.indexOf("-")>0
              [result.name, result.neighborhood] = element.name.split('-').map (element)-> element.trim()
            else
              result.neighborhood = element.name.trim()

            result.neighborhood += " area"
            result


          return callback(null,results)

        catch error

          $ = $root.cheerio.load(body)
          errors = $(".errorItemBox")
          if errors.length > 0
            message = errors.map (index, element)->
              $(element).text()

            new_error = new $root.Base.CrawlerHttpError(message.join("\n"),
              response.statusCode,
              response.headers,
              body,
              request_options)

          else
            new_error =  new $root.Base.ParserError("Error triying to do the express deal request:",
              self.name,
              body)

          new_error.trace_back = error.stack
          return callback new_error

    if ext_options.jsk?
      execute_query(ext_options, ext_callback)
    else
      self.get_initial_params ext_options, (error, new_options)->

        if error
          return ext_callback error

        execute_query(new_options, ext_callback)

