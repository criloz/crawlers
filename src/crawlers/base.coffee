"""
Base crawler module
===================

The objective of this module is implement all the basic method (http request, parser), base exceptions catching, and the
interface for access to all the crawlers

This module contains:
  * Base exceptions, and exception hierarchy
  * Base crawler class
  * Base request method
  * Caching must be here
  * Default interface for interact with the crawlers (search, stop)
  * Capture default  http exeptions  (status code > 400) or timeouts
  * Sessions specifications (how manage the cookies)

✔ Each crawlers  must be a subclass of this module
"""

$root = {}
$root.request = require "request"
$root.moment = require "moment"
$root.redis = require "redis"


"""
************************************************************************************************************************
                                            Exceptions definition
************************************************************************************************************************
"""

class BaseException extends Error

  """
    root of all the others exception
  """

  constructor:(@message)->
    @code = @constructor.name
    @status = "error"
    Error.captureStackTrace(this, arguments.callee);

"""
----------------------------------------------- internal exceptions ----------------------------------------------------
"""
class InternalError extends BaseException
  """
  Base exception for all the exception generated for the script internally
  """
  constructor:(message, @module)->
    super message
    @type = "InternalError"

class NoImplementedError extends  InternalError

class InterruptError extends InternalError
  """This exception is raise when the request methos is locked"""

class ParserError extends InternalError
  """
    Parent exception for all the parser errors
  """
  constructor:(message, module, @body)->
    super message, module

"""
------------------------------------------------ external exceptions----------------------------------------------------
"""
class HttpError extends BaseException
  """
  Base exception for all the exception generated due a http request
  """
  constructor:(message, @status_code, @response_header, @body, @options)->
    super message
    @type = "HttpError"

class HttpStatusError  extends HttpError
  """
  if status code > 400 raise this exeption
  """

class HttpTimeoutError  extends HttpError
  """
  raise when request  timeout
  """

class HttpUnknownError  extends HttpError
  """
  raise when unknow error
  """

class CrawlerHttpError extends HttpError
  """
  Exception for error derivates of requests to external site done by the crawlers
  """

"""
-------------------------------------------------- client exceptions----------------------------------------------------
"""
class QueryError  extends BaseException
  """
  Base exception for all the exception generated due a http request
  """
  constructor:(message, @errors)->
    super message
    @type = "QueryError"


"""
************************************************************************************************************************
                                                 Base class definition
************************************************************************************************************************
"""

module.exports = class

  """
    Crawler base class
  """

  @BaseException: BaseException
  @HttpError: HttpError
  @InternalError: InternalError
  @HttpStatusError: HttpStatusError
  @QueryError: QueryError
  @HttpTimeoutError: HttpTimeoutError
  @HttpUnknownError: HttpUnknownError
  @NoImplementedError: NoImplementedError
  @CrawlerHttpError: CrawlerHttpError
  @ParserError: ParserError
  @InterruptError:InterruptError

  lock_future_request: false

  constructor:->
    """
      each new instance have its own cookie
      a = new HotWire()
      b = new HotWire()
      a.cookier_jar != b.cookier_jar
    """
    @cookie_jar = $root.request.jar(arguments.callee)

  is_not_null:(input, varname)->
    """
      check if input is null or undefined
      return true in case that the input is not null
      return a error object in other cases
    """
    if not input?
      error =
        params: [varname]
        message: "undefined"
      return error
    return true


  is_int:(options)->
    """
        validate if a string is a integer,
        also check min and max limit if this options were provided

        return [true,val] in case that the input is a intenger
        return [a error object,null] in other cases
    """

    input = options.input
    varname = options.varname
    min = options.min
    max = options.max

    if typeof input == "string"
      if input.trim()==''
        input=0
    if not varname?
      throw Error("varname must be provided")

    """check if is not null"""
    is_not_null = @is_not_null(input, varname)

    if is_not_null==true
      """check if is a integer"""
      if not /^[0-9]+$/.test(input)
        error =
          params: [varname]
          message: "only numbers allowed"
          input: input
        return [error, null]

      number = parseInt(input)

      if min?
        if number < min
          error =
            params: [varname]
            message: varname+" must be greater than or equal to "+ min
            input: input
          return [error, null]

      if max?
        if number > max
          error =
            params: [varname]
            message: varname+" must be less than or equal to "+ max
            input: input
          return [error, null]

      return [true, number]

    else
      return  [is_not_null, null]

  is_date:(options)->

    """
      validate a date in the format YYYY-MM-DD

      return [true, date in ms] in case that the input is a intenger
      return [a error object, null] in other cases
    """

    input = options.input
    varname = options.varname

    if not varname?
      throw Error("varname must be provided (is_date)")

    """check if is not null"""
    is_not_null = @is_not_null(input, varname)
    if is_not_null==true
      """check if is a date"""
      matches =input.match /^(\d\d\d\d)-(\d?\d)-(\d?\d)$/

      if not matches? or matches.length!=4
        error =
          params: [varname]
          message: "Enter a valid date in the format (YYYY-MM-DD)."
          input: input
        return [error, null]

      year = parseInt(matches[1])
      month = parseInt(matches[2])-1
      day = parseInt(matches[3])
      date = new Date(year,month,day)

      if date.getDate()!=day or date.getFullYear()!=year or date.getMonth()!=month
        error=
          params: [varname]
          message: "Invalid date"
          input: input
        return [error, null]

      date_ms = date.getTime()
      return [true,date_ms]
    else
      return  [is_not_null,null]

  validate:(options)->

    errors = []

    if not options.location?
      error =
        params: ["location"]
        message: "undefined"
      errors.push(error)

    else
        if not /^[^,]+(, [^,]+)?$/.test(options.location)
          error =
            params: ["location"]
            message: "location must be defined in the format City[, Cuntry or State]"
            input: options.location
          errors.push(error)

    [s_is_date, s_date] = @is_date({input:options.start_date,varname:"start_date"})
    [e_is_date, e_date] = @is_date({input:options.end_date,varname:"end_date"})

    if s_is_date==true and e_is_date==true
      today = $root.moment($root.moment().format("YYYY-MM-DD"),"YYYY-MM-DD")._d.getTime()
      if s_date<today
        error=
          params: ["start_date"]
          message:"Should not be smaller than today"
          input:options.start_date
        errors.push(error)

      else if e_date<=s_date
        error=
          params: ["end_date", "start_date"]
          message:"Should be largest than start_date"
          input:options.end_date

        errors.push(error)

    else
      errors.push(s_is_date)
      errors.push(e_is_date)

    #validate defualt params
    [children_pass, children_value] = @is_int({input: options.children, varname: "children", min: 0})
    errors.push(children_pass)
    [guests_pass, guests_value] = @is_int({input: options.guests, varname: "guests", min: 0})
    errors.push(guests_pass)
    errors.push(@is_int({input: options.timeout, varname: "timeout", min: 5000})[0])# timeout min 5000

    #validate no defautl params
    errors.push(@is_int({input: options.rooms, varname: "rooms", min: 1})[0])
    [adults_pass, adults_value] = @is_int({input: options.adults, varname: "adults", min: 0})
    errors.push(adults_pass)

    if (adults_pass==true and guests_pass==true and children_pass==true)
      if guests_value!=0 and (children_value!=0 or adults_value!=0 )
        if  guests_value != (adults_value+children_value)
          error=
            param:["guests","children","adults"]
            message: "If guest is defined, should be equal to (children+adults)"
          errors.push(error)
      if (guests_value + children_value + adults_value)<=0
        error=
          params:["guests","adults","children"]
          message: "The group of people should be greater than one"
        errors.push(error)

    errors = errors.filter (x)-> x!=true
    return errors

  translate:(options, callback)->
    """
      this method is useful for translate and caching city names
      between alltherooms.com  and the other sites
    """
    self = @

    if options.redis? and options.redis.host? and options.redis.port? and options.city_internal_id?

      client = $root.redis.createClient(options.redis.port, options.redis.host)
      client.on "error", (err)->
        console.log("Error " + err)

      #check internal id in redis
      client.get "#{options.city_internal_id}:#{self.domain}", (err, reply)->
        if err?
          new_error = new InternalError "Error triying to use cache for city name", self.name
          new_error.trace = err
          client.end()
          return callback new_error

        if reply?
          #use cache
          options.location_translated = reply
          client.end()
          return callback null, options

        else
          #translate on demand and store the result in the cache database
          callback_with_caching = (err, new_options)->
            if err
              client.end()
              return callback err

            #store the result in redis
            client.set "#{new_options.city_internal_id}:#{self.domain}", new_options.location_translated, (err, reply)->
              if err
                new_error = new InternalError "Error triying to use cache for city name", self.name
                new_error.trace = err
                client.end()
                return callback new_error
              client.end()
              return callback null, new_options

          return self.translate_on_demand(options,callback_with_caching)

    else
      @translate_on_demand(options,callback)

  translate_on_demand:(options, callback)->
    callback new NoImplementedError "translate_on_demand method must be implemented", @name

  search:(options, input_callback)->
    """
      interface for access from external scripts to the crawler search methods
      if a method is not specified in  option.search_type alwasy will call the search_hotel method
    """
    self = @
    hashkey = options.hashkey
    service = options.service

    callback = (error,reply,paging)->
      input_callback error, reply, paging, hashkey, service

    query = options.query
    #define optional params
    if query.guests?
      """If adult and child are missing, use guests parameter"""
      query.adults?='0'

    #set default params if they not were defined
    query.children?='0'
    query.guests?='0'
    query.timeout?='5'

    """transform timeout from seconds to miliseconds"""
    if query.timeout?
      query.timeout = parseFloat(query.timeout) * 1000


    """Validate inputs"""
    errors = @validate(query)
    if errors.length > 0
      return callback new QueryError 'Error in the requests params', errors

    """Translate params"""
    @translate query,(error,translated_query)->

      if error
        return callback error

      """ defualt method to call is search_hotel"""
      if service?
        search_type = service.split(":")[1]
      else
        search_type = "search_hotel"
      self[search_type](translated_query, callback)

  stop:()->
    """
        General  method  for stop a crawler


        This method interrupt all the future call of the request method
        raise a Internal exception called  InterruptError that should be
        take in consideration inner the callback function enter as input of
        any search method, or any method that use internally the request method.


    """
    @lock_future_request = true


  request:(options, callback)->
    """
      request basic method
      use for set generall things
      * proxies
      * general options
      * catching generall exceptions
      * manage sessions
    """
    if @lock_future_request
      return callback new InterruptError("This istance can not use again the request method", @name)

    self = @
    options.jar?= @cookie_jar
    options.timeout?= 5000
    options.followAllRedirects?= true
    options.headers?= {}
    options.encoding?="utf8"
    options.headers["User-Agent"]?= "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.22 (KHTML, like Gecko) Ubuntu
 Chromium/25.0.1364.160 Chrome/25.0.1364.160 Safari/537.22"

    $root.request options, (error, response, body)->

      if error
        if error.code = "ETIMEDOUT"
          return callback new HttpTimeoutError "timeout", undefined, undefined, undefined, options
        else
          return callback new HttpUnknownError "unknown error", undefined, undefined, body, options

      response.url = response.request.uri.href
      self.name = self.name||response.request.uri.hostname

      if response.statusCode >= 400 < 500
        message = "Did a bad request to: " + self.name
        return callback new HttpStatusError message, response.statusCode, response.headers, body, options

      if response.statusCode >= 500
        message = self.name + ": Internal server error"
        return callback new HttpStatusError message, response.statusCode, response.headers, body, options

      callback null, response, body



