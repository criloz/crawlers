$root = {}
$root.Base = require "./base"
$root.cheerio = require "cheerio"
$root.querystring = require "querystring"

module.exports = class extends $root.Base

  """
    Booking scrapper
  """

  name: "Booking"
  domain: "booking.com"
  search_hotel_url: "http://www.booking.com/searchresults.html"

  get_initial_params: (options, callback)->

    """
        get cookies  and forms params
        return [error], jsk, fls and body
    """

    self = @

    request_options=
      timeout:options.timeout
      uri:"http://www.booking.com/index.html"

    @request request_options, (error, response, body)->

      if error
        return callback error

      if response.statusCode != 200
        return callback new $root.Base.CrawlerHttpError("Unknown error",
          response.statusCode,
          response.headers,
          body,
          request_options)

      try
        $ = $root.cheerio.load(body)
        form = $('#frm')
        options.src = form.find("input[name=src]").attr("value")
        options.nflt =  form.find("input[name=nflt]").attr("value")
        options.error_url = form.find("input[name=error_url]").attr("value")
        options.dcid = form.find("input[name=dcid]").attr("value")
        options.sid= form.find("input[name=sid]").attr("value")
        options.si=  form.find("input[name=si]").attr("value")
        options.aid = $("input[name=aid]").attr("value")
        options.dest_type = "city"

        [s_y,s_m,s_d] = options.start_date.split("-")
        [e_y,  e_m, e_d] = options.end_date.split("-")
        options.checkin_monthday = s_d
        options.checkin_year_month = s_y+"-"+s_m
        options.checkout_monthday = e_d
        options.checkout_year_month = e_y+"-"+e_m
        return  callback null, options, body

      catch error
        new_error =  new $root.Base.ParserError("Error triying to do the express deal request:",
          self.name,
          body)
        new_error.trace_back = error.stack
        return callback new_error

  translate_on_demand: (input_options, callback)->

    self = @
    @get_initial_params input_options, (error, options)->
      if error
        return callback error
      query =
        lang: "en"
        sid: options.sid
        aid: options.aid
        term: options.location.split(",")[0].trim()

      request_options=
        qs: query
        timeout:options.timeout
        uri:"http://www.booking.com/autocomplete"

      self.request request_options, (error, response, body) ->
        if error
          return callback error

        if response.statusCode != 200
          return callback new $root.Base.CrawlerHttpError("Unknown error",
            response.statusCode,
            response.headers,
            body,
            request_options)
        try

          json_body = JSON.parse body

          if not json_body.city?  or json_body.city.length == 0

            new_error = new $root.Base.CrawlerHttpError("Bad input city for booking",
              response.statusCode,
              response.headers,
              body,
              request_options)
            return callback new_error
          options.location_translated = json_body.city[0].dest_id
          return callback null, options

        catch error
          new_error =  new $root.Base.ParserError("Error triying to obtain the city id:", self.name, body)
          new_error.trace_back = error.stack
          return callback new_error

  search_hotel:(ext_options, ext_callback)->

    self = @

    execute_query = (options, callback)->

      options.offset?=0

      query=
        src: options.src
        nflt: options.nflt
        error_url: options.error_url
        dcid: options.dcid
        sid: options.sid
        si: options.si
        dest_type: options.dest_type
        dest_id: options.location_translated
        selected_currency:'USD'
        rows: 50
        offset:options.offset
        checkin_monthday: options.checkin_monthday
        checkin_year_month: options.checkin_year_month
        checkout_monthday: options.checkout_monthday
        checkout_year_month: options.checkout_year_month
        in_a_group:'on'
        org_nr_rooms:  options.rooms
        org_nr_adults:  options.adults
        org_nr_children: options.children

      querystring = $root.querystring.stringify(query)
      guests = parseInt(options.adults)  +   parseInt(options.children)
      rooms =   parseInt(options.rooms)
      remain = guests % rooms
      each_room =  (guests - remain) / rooms

      for i in new Array(rooms)
        add = 0
        if remain!=0
          add = 1
          remain -= 1

        group =
          group_adults: each_room + add
          group_children:0

        querystring += '&' + $root.querystring.stringify(group)

      request_options =
        uri: self.search_hotel_url+'?'+ querystring
        timeout: options.timeout

      self.request request_options, (error, response, body) ->
        if error
          return callback error

        if response.statusCode != 200
          return callback new $root.Base.CrawlerHttpError("Unknown error",
            response.statusCode,
            response.headers,
            body,
            request_options)

        try
          $ = $root.cheerio.load(body)
          results = $('div.flash_deal_soldout').map (index, element) ->
            element = $(element)
            result = {}
            result.rating = element.find("span.average").text().trim()
            result.stars = parseFloat(element.attr("data-stars"))
            result.name = element.find("a.hotel_name_link").text().trim()
            result.deeplink = 'http://www.booking.com' + element.find("a.hotel_name_link").attr("href")
            neighborhood = element.find("div.address").find("a[rel=200]").text()
            if neighborhood!=""
              result.neighborhood = neighborhood.replace(new RegExp(" map$"),'')
                .replace(new RegExp("^[0-9]+\. "),'').trim()   + " area"
            result.price = element.find("div.total").find("strong").find("span").text().replace("$",'').replace(",",'.')
              .trim()

            if result.price == ''
              result.price = element.find("strong.price").text().replace("$",'').replace(",",'.')
                .trim()

            result.price = parseFloat(result.price)


            result

          if results.length != 0
            paging = true

          options.offset += 50

          callback null, results, paging

          if paging
            execute_query options, callback


        catch error
          new_error =  new $root.Base.ParserError("Error triying to parse the hotel in search_hotel:", self.name, body)
          new_error.trace_back = error.stack
          return callback new_error

    #check if translated_on_demand method was used or not
    if ext_options.dcid?
      execute_query(ext_options, ext_callback)
    else
      self.get_initial_params ext_options, (error, new_options)->

        if error
          return ext_callback error

        execute_query(new_options, ext_callback)