$root = {}
$root.Base = require "./base"
$root.cheerio = require "cheerio"
$root.moment = require "moment"


module.exports = class extends $root.Base

  """
      HostWire scrapper
  """

  name: "HostWire"
  domain: "hotwire.com"
  search_url: "http://www.hotwire.com/hotel/search-options.jsp"

  translate_on_demand: (options, callback)->
    options.location_translated =   options.location
    return callback null, options

  parse_dates: (options)->
    #translate dates
    """YYYY-MM-DD ->  MM/DD/YYYY"""
    options.start_date = $root.moment(options.start_date, "YYYY-MM-DD").format("MM/DD/YYYY")
    options.end_date = $root.moment(options.end_date, "YYYY-MM-DD").format("MM/DD/YYYY")

  search_hotel:(options, callback)->

    self = @
    self.parse_dates(options)
    #query params
    query =
      inputId: 'index'
      rs: 0
      contextDomId: 'genRandom1'
      isFromSearchPopup: 'false'
      destCity: options.location_translated
      startDate: options.start_date
      endDate: options.end_date
      numRooms:options.rooms
      noOfAdults: options.adults
      noOfChildren: options.children
      selectedPartners: 'HO'

    #request options
    request_options =
      uri: @search_url
      qs: query
      followRedirect: true
      setEncoding: 'utf8'
      timeout: options.timeout


    @request request_options, (error, response, body)->
      if error
        return callback error

      if response.statusCode == 200


        if body.indexOf("Please select from the match(es) shown below") >= 0
          return callback new $root.Base.CrawlerHttpError("Bad city name"
            response.statusCode,
            response.headers,
            body,
            request_options)

        $ = $root.cheerio.load(body)

        if $('.errorPage').length != 0
          if body.indexOf("still busy processing your previous") >= 0
            return callback new $root.Base.CrawlerHttpError("We are still busy processing the previous hotel search",
              response.statusCode,
              response.headers,
              body,
              request_options)
          else
            return callback new $root.Base.CrawlerHttpError("Unknown error",
              response.statusCode,
              response.headers,
              body,
              request_options)

        else if (error_messages = $('.errorMessages')).length != 0
          return callback new $root.Base.CrawlerHttpError(error_messages.text(),
            response.statusCode,
            response.headers,
            body,
            request_options)
        else
          request_options =
            uri: "http://www.hotwire.com/hotel/results.jsp?actionType=999&isAjaxRequest=true&pageDef=tiles-def.hotel.results&xxxxxxxx=results&xxxxxxxx=tabs_resultTabsTile&xxxxxxxx=A1&xxxxxxxx=A3&xxxxxxxx=A5&xxxxxxxx=B1&xxxxxxxx=B2&xxxxxxxx=B4&xxxxxxxx=B5&xxxxxxxx=shoppingtools&xxxxxxxx=disclaimerText&xxxxxxxx=surveyLayer&xxxxxxxx=proactiveChat&xxxxxxxx=callHelpTopModule"

          closure = ()->
            """
              useful trick for wait till hostwire have prepared the results
              if not body intent again each second
            """
            self.request request_options, (error, response, body)->
              if error
                return callback error
              if response.statusCode == 200
                $ = $root.cheerio.load(body)
                try
                  results = $('div.singleResult').map (index, element) ->
                    element = $(element)
                    result =
                      neighborhood: element.find(".neighborhoodName a").text().replace("area hotel","area").trim()
                      price: parseFloat(element.find(".price").text().replace("$","").trim())
                      stars: parseFloat(element.find(".goldStarRating span").attr("title").match(/([0-9.]+) out of 5.0/)[1])

                    rating = element.find(".recommendedText strong").text().trim()
                    unless rating== '' then result.rating = rating

                    return result
                catch error
                  return callback new $root.Base.ParserError(error.message,self.name, body)

                callback null, results

              else if response.statusCode == 204
                setTimeout(closure,1000)

              else
                return callback new $root.Base.CrawlerHttpError("Unknown error",
                  response.statusCode,
                  response.headers,
                  body,
                  request_options)

          return closure()
      else
        return callback new $root.Base.CrawlerHttpError("Unknown error",
          response.statusCode,
          response.headers,
          body,
          request_options)