$root = {}
$root.path = require 'path'
$root.fs = require 'fs'
express = require "express"
app = express()
$root.server = require('http').createServer(app)
$root.io = require('socket.io').listen($root.server)
$root.CrawlersProtocol = require './servers/crawlers_protocol'

uniqueId = (length=8) ->
  id = ""
  id += Math.random().toString(36).substr(2) while id.length < length
  id.substr 0, length

split_array = (array, n)->
  remain = array.length % n
  each =  (array.length-remain)/n
  new_arrays = []
  index = 0
  for i in new Array(n)
    tmp = []
    add = 0
    if remain!=0
      add = 1
      remain -= 1
    for j in new Array(each+add)
      tmp.push(array[index])
      index = index + 1
    new_arrays.push(tmp)
  new_arrays



lessMiddleware = require 'less-middleware'
services = {}
dev_servers = [3001,3002,3003]
clients = []

active_sockets = {}

all_services_connected = ->

  for client in clients

    client.on "crawl_search_success", (data)->
      if active_sockets[data.hashkey]?
        active_sockets[data.hashkey].emit("success", {results:data.results,site:data.service})

    client.on "crawl_search_error", (data)->
      if active_sockets[data.hashkey]?
        error = data.error
        error.site =  data.service
        active_sockets[data.hashkey].emit('error', error)

  $root.io.on 'error', (socket)->
    #stop all crawlers of this session
    socket.get 'session', (err, session)->
      if active_sockets[session]?
        for client in clients
          client.emit("stop", {hashkey: session})
      delete active_sockets[session]

  $root.io.on 'disconnect',(socket)->
    #stop all crawlers of this session
    socket.get 'session', (err, session)->
      if active_sockets[session]?
        for client in clients
          client.emit("stop", {hashkey: session})
      delete active_sockets[session]

  $root.io.on 'connection',(socket)->

    # send current services
    socket.emit('services', { services: Object.keys(services)})

    socket.on 'stop',->
      #stop all crawlers of this session
      socket.get 'session', (err, session)->
        if active_sockets[session]?
          for client in clients
            client.emit("stop", {hashkey: session})
          console.log("stoping all services for this session " + session)

    socket.on 'search',(form)->

      init_search = (session_key)->
        services_on = Object.keys(form.services_on).filter (x) -> form.services_on[x]
        delete form.services_on
        #equally divide the services between servers
        split_services  = split_array(services_on, clients.length)


        cindex = 0
        for services_group in split_services

          if services_group.length == 0
            continue

          client = clients[cindex]
          # set location internal id as the same location
          form.city_internal_id = form.location

          data =
            hashkey: session_key
            services: services_group
            query: form

          client.emit "crawl_search", data
          cindex += 1

      socket.get 'session', (err, session)->
        if active_sockets[session]?
          init_search(session)
        else
          #create session
          new_session = uniqueId(32)
          socket.set 'session', new_session, (err)->
            active_sockets[new_session] = socket
            init_search(new_session)

  app.set 'views', '../views'
  app.set 'view engine', 'jade'
  app.use express.logger('dev')
  app.use express.static('../public')
  app.use lessMiddleware {dest: '../public/css', src: '../less', prefix: '/css', compress: true}
  app.get '/', (req, res)->
    res.render("tester")

  $root.server.listen(3000)


###
  start clients
###

count = 0

for port in  dev_servers
  new_client = new $root.CrawlersProtocol()

  new_client.on "crawlers_discover", (data)->
    @emit("change_dbs", {dbs:{couchdb:{host:"127.0.0.1",port:5984}, redis:{host:"127.0.0.1",port:6379}}})
    services = data.services
    count += 1
    if count== dev_servers.length
      all_services_connected()

  new_client.connect('127.0.0.1',port)

  clients.push(new_client)
