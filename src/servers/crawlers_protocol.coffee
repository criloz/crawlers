$root = {}
$root.net = require 'net'
$root.nano = require 'nano'
class CrawlTcpError extends Error

  constructor:(@message)->
    @code = @constructor.name
    Error.captureStackTrace(this, arguments.callee);

module.exports = class

  """
    Implements protocol  that allow control the crawlers over network
    asynchronously
  """

  constructor:(@type='client')->

    self  = @
    @separator = "##alltherooms##"
    @on_data_event = {}
    @on_connection_events = []
    @on_error_events = []
    @on_close_events = []

    if @type=='server'

      @clients = {}
      @socket = $root.net.createServer()
      @socket.on 'connection', (client)->
        #assing uuid to the client
        uuid = self.unique_id()
        self.clients[uuid] = client

        #execute initial events
        for event in self.on_connection_events
          event(uuid)

        client.on "error", (e)->
          for event in self.on_error_events
            event(uuid)
          delete self.clients[uuid]

        client.on "close",->
          for event in self.on_close_events
            event(uuid)
          delete self.clients[uuid]

        client.on "data", (data)->
          data = data.toString()
          data = data.split(self.separator)
          for part in data
            if part == ""
              continue
            try
              json_obj = JSON.parse(part)
            catch e
              error = new CrawlTcpError("Error triying to parse the data")
              error.trace_back = e.stack
              try
                return client.write({event:"api_error", data:{error:error}})
              catch e
                return

            if json_obj.event?

              if self.on_data_event[json_obj.event]?
                self.on_data_event[json_obj.event](json_obj.data, uuid)
              else
                error = new CrawlTcpError("event that you emitited not exists in the server side")
                return client.write({event:"api_error", data:{error:error}})

            else
              error = new CrawlTcpError("event should have defined")
              return client.write({event:"api_error", data:{error:error}})


  unique_id:(length=16)->
    id = ""
    id += Math.random().toString(36).substr(2) while id.length < length
    id.substr 0, length

  listen:(@port=20136,callback) ->
    @socket.listen @port, ->
      console.log('alltherooms crawlers started')
      if callback?
        callback()

  on:(event, callback)->

    if event=="connection"
      @on_connection_events.push(callback)

    else if event=="error"
      @on_error_events.push(callback)

    else if event=="close"
      @on_close_events.push(callback)
    else
      @on_data_event[event] = callback

  connect:(@host, @port)->
    self = @

    @socket = $root.net.createConnection @port, @host, ()->

    @socket.on 'connect', ()->
      #execute initial events
      for event in self.on_connection_events
        event()

    @socket.on "error", (e)->
      for event in self.on_error_events
        event()

    @socket.on "close", ->
      for event in self.on_close_events
        event()

    @socket.on "data", (data)->
      data = data.toString()
      data = data.split(self.separator)
      for part in data
        if part == ""
          continue
        json_obj = JSON.parse(part)

        if json_obj.event?
          if self.on_data_event[json_obj.event]?
            self.on_data_event[json_obj.event].apply(self, [json_obj.data])


  emit:(event, data, uuid)->
    #avoid emit null data
    data?={}
    self = @
    if @type=='server'

      if @clients[uuid]?

        @clients[uuid].write(JSON.stringify({event:event, data:data})+@separator)

        if event=="crawl_search_error"
          #if couhdb is defined save the error in the database
          if @couchdb? and @couchdb.host? and @couchdb.port?
            nano = $root.nano("http://#{@couchdb.host}:#{@couchdb.port}")
            nano.db.create 'errors', ->
              # specify the database we are going to use
              errors = nano.use 'errors'
              #and insert a document in it
              date = new Date()
              data.date = date.getTime()
              errors.insert data, (err, body, header)->
                if err
                  self.clients[uuid].write(JSON.stringify({event:"api_error", data: new Error(err.message)})+self.separator)
                #useful for testing
                self.clients[uuid].write(JSON.stringify({event:"couchdb_error_saved", data:{id:body.id}})+self.separator)
    else
      @socket.write(JSON.stringify({event:event, data:data})+@separator)