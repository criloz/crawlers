$root = {}
$root.CrawlersProtocol = require './crawlers_protocol'
$root.fs = require 'fs'
$root.path = require 'path'
$root.Base = require '../crawlers/base'

module.exports = class extends $root.CrawlersProtocol

  """
      This module contains a tcp server that allow comunication with the crawler over the network  """

  crawlers_path: '../crawlers'

  constructor:->

    #initialize
    super 'server'

    self = @
    #load crawlers
    [@services, @crawlers]  = @crawl_discover('../crawlers')
    @active_searchs = {}
    @keys = {}
    #suscribe events
    @on "connection", (uuid)->
      self.emit "crawlers_discover", {services:self.services}, uuid

    @on "crawl_search", (data, uuid)->
      self.crawl_search data.hashkey, data.services, data.query, uuid

    @on "stop", (data, uuid)->
      self.stop data.hashkey, uuid

    @on "error", (data, uuid)->
      self.stop null, uuid
      delete self.keys[uuid]

    @on "close", (data, uuid)->
      self.stop null, uuid
      delete self.keys[uuid]

    @on "change_dbs", (data, uuid)->
      self.change_dbs data.dbs, uuid


  crawl_discover:->

    """
        This function search crawler scripts over the give path
        and register its avaliable services

        services: are crawler method that start with the keyword searh_

        return-> [services: Object, Crawlers:Object] dictionaries that contains services
        and the crawlers constructors
    """
    services = {}
    crawlers = {}
    path = $root.path.normalize($root.path.join(__dirname, @crawlers_path))

    for file in $root.fs.readdirSync path

      if file == "base.coffee"
        """ignore base class"""
        continue

      else if $root.path.extname(file) != ".coffee"
        """Ignore not coffee files"""
        continue

      else
        """discover all the crawlers"""
        abs_path = $root.path.join(path, file)
        new_crawler = require abs_path
        properties  = Object.keys(new_crawler.prototype)
        for property in properties
          if property.indexOf("search_") == 0
            if typeof new_crawler.prototype[property] == "function"
              new_service = new_crawler.prototype.name.toLowerCase() + ":" + property
              services[new_service] = {domain:new_crawler.prototype.domain, hostname:new_crawler.prototype.name}
              crawlers[new_service] = new_crawler

    return [services, crawlers]

  clone : (obj) ->
    """
        clone a object in js
        TODO: find a most efficient way
    """
    JSON.parse JSON.stringify(obj)


  is_search_complete:(hashkey, uuid)->
    """
      if the search is complete emit a event a clean up the hashkey
    """
    if Object.keys(@active_searchs[hashkey].services).length == 0
      #all the search related to this hashkey have ended
      @emit("crawl_search_complete", {hashkey:hashkey, stopped:@active_searchs[hashkey].stopped}, uuid)
      if @active_searchs[hashkey].request_on_queue?
        #if exist a request in the queue excute it
        request_on_queue =  @active_searchs[hashkey].request_on_queue
        delete @active_searchs[hashkey]
        delete @keys[uuid][hashkey]
        @crawl_search(request_on_queue.hashkey,request_on_queue.services, request_on_queue.query,uuid)
      else
        delete @active_searchs[hashkey]
        delete @keys[uuid][hashkey]

  stop:(hashkey=null, uuid)->
    """
    stop a search by hashkey if this is given
    otherwise stop the searchs globally
    """
    if hashkey?
      if @active_searchs[hashkey]?
        for active_service in  Object.keys(@active_searchs[hashkey].services)
          @active_searchs[hashkey].services[active_service].stop()
    else
      if @keys[uuid]?
        for hashkey in Object.keys(@keys[uuid])
          @stop(hashkey)

  change_dbs:(dbs, uuid)->
    """
        useful for change yhe db that the crawlers will use for
        store error log -> couchdb
        caching citynames -> redis
    """
    @couchdb = if dbs?.couchdb? then dbs.couchdb else @couchdb
    @redis = if dbs?.redis? then dbs.redis else @redis

  crawl_search:(hashkey, services, query, uuid)->

    """
        Start a new search

        inputs

            hashkey(sring): due the async nature, useful for identify the search result
                            in the client side, and create search with diferent cookie sessions


            services(list): list of services that will be used

            query(dict): query params for the search, city, dates, rooms, etc

        The hashkey is useful for do search through  the same site
        usign diferent cookies session. Some sites use the cookie session for
        show its results, in orden to not merge concurrent search is necesary do
        this type of separation

        Also if there exist a query identified with a hashkey, and come another request
        usign the same hashkey, the first query will be canceled


        emit
            crawl_search_error  if exist a error when the crawler perform the search or in the query
            crawl_search_success if all its ok
            crawl_search_endeded when a search ended, emit a event (crawl_search_ended) with the hashkey that identified that search
    """
    self = @
    #check if hashskey != null and string
    if not hashkey?  or typeof hashkey != "string"
      error = new $root.Base.QueryError("hahskey should be defined and should be a string")
      self.emit "crawl_search_error",{error:error, service:null, hashkey:null, paging:false}, uuid
      return

    if not services? or not (services instanceof Array)
      error = new $root.Base.QueryError("services should be defined and should be a list")
      self.emit "crawl_search_error",{error:error, service:null, hashkey:hashkey, paging:false}, uuid
      return

    if not query? or typeof query != "object"
      error = new $root.Base.QueryError("query should be defined and should be a dictionary")
      self.emit "crawl_search_error",{error:error, service:null, hashkey:hashkey, paging:false}, uuid
      return

    if not self.keys[uuid]?
      self.keys[uuid] = {}

    if not self.keys[uuid][hashkey]?
      self.keys[uuid][hashkey] = true

    #check if hashkey have active searchs
    if @active_searchs[hashkey]?
      #stop all the crawlers attached to that hashkey
      @stop(hashkey, uuid)
      #put the current query in the queue
      @active_searchs[hashkey].request_on_queue = {hashkey:hashkey, services:services, query:query}
    else
      @active_searchs[hashkey] = {services:{}, stopped:false}

      for service in services

        #if the service not exist emit error
        if not @crawlers[service]?
          error = new $root.Base.QueryError("The service #{service} not exist")
          self.emit "crawl_search_error", {error:error, service:null, hashkey:hashkey, paging:false}, uuid
          continue

        crawler = new @crawlers[service]

        @active_searchs[hashkey].services[service] = crawler

      self.is_search_complete(hashkey, uuid)

      #execute queries
      for service in services
        new_query = @clone(query)
        if self.redis?
          new_query.redis = self.redis

        crawler = @active_searchs[hashkey].services[service]

        crawler.search {query:new_query, hashkey:hashkey, service:service}, (error, reply, paging, hashkey, service)->

          paging?=false

          if not self.active_searchs[hashkey].stopped
            # if the request was stopped not have sense continue  emitting events
            if error
              #if error code is InterruptError, that means that  the crawler was stoped
              # so not emit a signal if this happen
              if !(error.code == "InterruptError")
                self.emit 'crawl_search_error', {error:error, service:service, hashkey:hashkey, paging:paging}, uuid
              else
                #mark the search query as stoped by the client
                self.active_searchs[hashkey].stopped = true
            else
              self.emit "crawl_search_success", {results:reply,service:service, hashkey:hashkey, paging: paging}, uuid

            if not paging
              # if not exist pagging  mean that the search end
              delete self.active_searchs[hashkey].services[service]

            self.is_search_complete(hashkey, uuid)