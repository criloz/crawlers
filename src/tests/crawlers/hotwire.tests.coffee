HostWires = require "../../crawlers/hotwire"
cheerio = require "cheerio"
moment = require "moment"
redis = require "redis"

exports.HotWireTestCases =

  "Test simple request":(test)->
    crawler = new HostWires
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)
    client = redis.createClient(6379, '127.0.0.1')
    crawler_query =
      location: "Paris, France"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20
      redis: {host: "127.0.0.1", port: 6379}
      city_internal_id: "hotwire_test"

    crawler.search  {query: crawler_query},
    (error, result)->
      test.equals(error, null)
      #clean db
      client.del "hotwire_test:hotwire.com", (err, reply)->
        client.end()
        test.done()

  "Test when is enter a city is not completely defined":(test)->
    crawler = new HostWires

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler_query =
      location: "Wonderland" # hotwire ask for many cities in this case, this should return a error
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    """ call directly search_hotel
    becouse this test is about how respond the site when exist
    a  bad input
    """
    crawler.search_hotel crawler_query,
    (error, response, body)->
      test.ok(error instanceof HostWires.CrawlerHttpError)
      test.done()



  "Test when exist a error in the start_date or end_date on the query":(test)->
    crawler = new HostWires

    crawler_query =
      location: "Paris, France"
      start_date: "dfsd"
      end_date: "dfsdfsd"
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    """ call directly search_hotel
        becouse this test is about how respond the site when exist
        a  bad input
    """

    crawler.search_hotel crawler_query,
    (error, response, body)->
      test.ok(error instanceof HostWires.CrawlerHttpError)
      test.done()

  "Test when is enter a bad city":(test)->
    crawler = new HostWires
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)


    crawler_query =
      location: "OmicronPer6_8"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000
    """ call directly search_hotel
    becouse this test is about how respond the site when exist
    a  bad input
    """
    crawler.search_hotel crawler_query,
    (error, response, body)->
      test.ok(error instanceof HostWires.CrawlerHttpError)
      test.done()


  "Test William Beckler design spec for hostwire":(test)->
    crawler = new HostWires
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler_query =
      location: "New York City, NY"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    isNumber = (o)->
      typeof o == 'number' and isFinite(o);

    crawler.search {query: crawler_query},
    (error, reply)->
      for i in reply
        test.ok(isNumber(i.stars), "stars Should  be a number")
        test.ok(isNumber(i.price), "price Should  be a number")
        min_defined = if i.neighborhood? and  i.stars? and i.price? then true else false
        test.ok min_defined, "Should be defined at last neighborhood,stars and price"
        if i.rating?
          test.ok /[0-9.%]/.test(i.rating), "If rating is defined Should be in this formar -> 'number%' "
      test.done()


