$root = {}
$root.Base = require "../../crawlers/base"
$root.coffee = require "coffee-script"
$root.fs = require "fs"
$root.path = require "path"
moment = require "moment"
$root.redis = require "redis"

exports.BaseTestCases =

  "Test when city name cache on demand":(test)->

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    city_internal_id = "0987"
    crawler =  new $root.Base()
    crawler.domain = "hotwire.com"

    client = $root.redis.createClient(6379, '127.0.0.1')

    crawler_query =
      start_date: moment(date).format('YYYY-M-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      location: "Bogota, Colombia"
      rooms: '1'
      guests: '0'
      adults: '2'
      children: '0'
      timeout: 20
      redis: {host: "127.0.0.1", port: 6379}
      city_internal_id: city_internal_id

    #simulate translate on demand
    crawler.translate_on_demand = (options, callback)->
      options.location_translated = "Paris, France"
      callback null, options


    crawler.translate crawler_query, (error, new_options)->

      test.equal(new_options.location_translated, "Paris, France", "Translate on demand should change the location to Paris, France")

      #check if the city name was cached on redis
      client.get "#{city_internal_id}:hotwire.com", (err, reply)->
        test.equal(reply, "Paris, France", "The result should be cached on redis")
        #clean db
        client.del "#{city_internal_id}:hotwire.com", (err, reply)->
          client.end()
          test.done()

  "Test when there exists a cached cityname":(test)->
   # store the cache

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler =  new $root.Base()

    city_internal_id = "0123"

    client = $root.redis.createClient(6379, '127.0.0.1')

    client.set "#{city_internal_id}:hotwire.com", "Madrid, Spain", (err, reply)->


      crawler_query =
        start_date: moment(date).format('YYYY-M-DD')
        end_date: moment(date2).format('YYYY-MM-DD')
        location: "Bogota, Colombia"
        rooms: '1'
        guests: '0'
        adults: '2'
        children: '0'
        timeout: 20
        redis: {host: "127.0.0.1", port: 6379}
        city_internal_id: city_internal_id

      crawler.domain = "hotwire.com"

      crawler.translate crawler_query, (error, new_options)->
        test.equal(error, null, "Should not have error")
        test.equal(new_options.location_translated, "Madrid, Spain", "The location should be changed to Madrid, Spain")

        #clean db
        client.del "#{city_internal_id}:hotwire.com", (err)->
          client.end()
          test.done()

  "Test when a bad redis host and port is used":(test)->
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)
    crawler =  new $root.Base()

    crawler_query =
      start_date: moment(date).format('YYYY-M-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      location: "Bogota, Colombia"
      rooms: '1'
      guests: '0'
      adults: '2'
      children: '0'
      timeout: 20
      redis: {host: "saaf", port: "sds"}
      city_internal_id: "1213"

    crawler.search {query: crawler_query}, (error)->
      test.ok error.trace=="Redis connection to saaf:sds failed - connect ENOENT"
      test.done()

  "Test stop function: the function should stop the future call of the request methods": (test)->
    counter = 0
    crawler = new $root.Base()
    closure = ()->
      crawler.request {uri: "http://www.google.com"},
      (error, response, body)->
        if error instanceof $root.Base.InterruptError
          #end the test
          return test.done()

        test.equals(response.statusCode, 200)
        # add +1 to counter
        counter += 1
        if counter == 3
          crawler.stop()
        closure()
    closure()

  "Test bad(rooms,guests,adults,children, timeout) ": (test)->
    """ tests for the is_int validator method """

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    #undefined dates
    crawler_query =
      start_date: moment(date).format('YYYY-M-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      location: "Bogota, Colombia"
      rooms: 'ed'
      guests: 'e'
      adults: 'a'
      children: '-1'
      timeout: '2ea'

    crawler = new $root.Base()
    errors = crawler.validate(crawler_query)
    test.ok errors.length==5
    test.done()

  "Test guest + adults + children == 0  ": (test)->
    """ tests for the is_int validator method """

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    #undefined dates
    crawler_query =
      start_date: moment(date).format('YYYY-M-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      location: "Bogota, Colombia"
      rooms: '1'
      guests: ''
      adults: '0'
      children: '0'
      timeout: '5000'

    crawler = new $root.Base()
    errors = crawler.validate(crawler_query)
    test.ok errors.length==1
    test.equal errors[0].message, "The group of people should be greater than one"
    test.done()

  "Test if guest is defined != (adults + children)": (test)->
    """ tests for the is_int validator method """

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    #undefined dates
    crawler_query =
      start_date: moment(date).format('YYYY-M-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      location: "Bogota, Colombia"
      rooms: '1'
      guests: '5'
      adults: '1'
      children: '1'
      timeout: '5000'

    crawler = new $root.Base()
    errors = crawler.validate(crawler_query)
    test.ok errors.length==1
    test.equal errors[0].message, "If guest is defined, should be equal to (children+adults)"
    test.done()

  "Test is_int method": (test)->
    """ tests for the is_int validator method """
    crawler = new $root.Base()
    test.deepEqual(crawler.is_int({input:2, varname: "Tester"}),[true,2])
    test.ok(crawler.is_int({input:'2s', varname: "Tester"})[1]==null)
    test.ok(crawler.is_int({input:'004', varname: "Tester"})[1]==4)
    test.ok(crawler.is_int({input:'', varname: "Tester"})[1]==0, " '' Should be taken as 0")
    test.done()

  "Test is_date method": (test)->
    """ tests for the is_date validator method """
    crawler = new $root.Base()
    test.equal(crawler.is_date({input:"1987-10-11",varname:"tester"})[0],true)

    test.equal(crawler.is_date({input:"2013-1-11",varname:"tester"})[0],true)
    test.equal(crawler.is_date({input:"1998-10-1",varname:"tester"})[0],true)
    test.equal(crawler.is_date({input:"1998-7-1",varname:"tester"})[0],true)

    test.notEqual(crawler.is_date({input:"199-7-1",varname:"tester"})[0],true)
    test.notEqual(crawler.is_date({input:"1998-17-1",varname:"tester"})[0],true)
    test.notEqual(crawler.is_date({input:"2013-02-32",varname:"tester"})[0],true)

    test.done()

  "Test validator with bad dates": (test)->

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    #undefined dates
    crawler_query =
      location: "Bogota, Colombia"
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    crawler = new $root.Base()
    errors = crawler.validate(crawler_query)
    test.equal(errors.length,2, 'Should only show two error (undefined start_date and end_date)')

    #start_date and end_date with bad format
    crawler_query =
      start_date: moment(date).format('YYYY/MM/DD')
      end_date: moment(date2).format('YYYY/MM/DD')
      location: "Bogota, Colombia"
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    crawler = new $root.Base()
    errors = crawler.validate(crawler_query)
    test.equal(errors[0].message,'Enter a valid date in the format (YYYY-MM-DD).', "the message should advice about bad format")
    test.equal(errors[1].message,'Enter a valid date in the format (YYYY-MM-DD).', "the message should advice about bad format")


    date = new Date(new Date().getTime() - 20 * 24 *60 *60 *1000)
    date2 = new Date(new Date().getTime() + 3 * 24 * 60 * 60 * 1000)
    #start_date < today
    crawler_query =
      start_date: moment(date).format('YYYY-M-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      location: "Bogota, Colombia"
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    crawler = new $root.Base()
    errors = crawler.validate(crawler_query)
    test.equal(errors.length,1,"only a error (start_date>today)")
    test.equal(errors[0].params[0],"start_date")
    test.equal(errors[0].message,"Should not be smaller than today")


    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(new Date().getTime() - 3 * 24 * 60 * 60 * 1000)

    #start_date > end_date
    crawler_query =
      start_date: moment(date).format('YYYY-M-DD')
      end_date: moment(date2).format('YYYY-MM-D')
      location: "Bogota, Colombia"
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    crawler = new $root.Base()
    errors = crawler.validate(crawler_query)
    test.equal(errors.length,1,"only a error (end_date>start_date)")
    test.deepEqual(errors[0].params,["end_date","start_date"])
    test.equal(errors[0].message,"Should be largest than start_date")

    test.done()

  "Test validator with bad city names": (test)->

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    #undefined city
    crawler_query =
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    crawler = new $root.Base()
    errors = crawler.validate(crawler_query)
    test.equal(errors.length,1, 'Should only show a error (undefined city)')
    test.equal(errors[0].params[0],'location', "Should be over the location param (undefined city)")
    test.equal(errors[0].message,'undefined', "the message should be undefined")

    #city with more of 2 ,
    crawler_query =
      location: "lls ss, ffff, eee"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    crawler = new $root.Base()
    errors = crawler.validate(crawler_query)
    test.equal(errors.length,1, 'Should only show a error (bad format city)')
    test.equal(errors[0].params[0],'location', "Should be over the location param (bad format city)")
    test.equal(errors[0].message,'location must be defined in the format City[, Cuntry or State]','(bad format city)')
    test.done()


  "Test validator with well formated cities": (test)->

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler_query =
      location: "New York USA"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    crawler = new $root.Base()
    errors = crawler.validate(crawler_query)
    test.equal(errors.length,0, 'Should not show a error')

    crawler_query =
      location: "New York, USA TT"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    crawler = new $root.Base()
    errors = crawler.validate(crawler_query)
    test.equal(errors.length,0, 'Should not show a error')

    test.done()

  "Test simple request":(test)->
    base = new $root.Base

    base.request {uri: "http://www.google.com"},
    (error, response, body)->
      test.equals(response.statusCode, 200)
      test.done()

  "Test create new exeptions":(test)->

    base = new $root.Base.BaseException()
    test.equals(base.code, $root.Base.BaseException.name)

    internal = new $root.Base.InternalError
    http = new $root.Base.HttpError
    query =  new $root.Base.QueryError

    test.ok(internal instanceof $root.Base.BaseException, "Internal error not is subclass of BaseExeption")
    test.ok(http instanceof $root.Base.BaseException, "Http error not is subclass of BaseExeption")
    test.ok(query instanceof $root.Base.BaseException, "Query error not is subclass of BaseExeption")

    test.ok(not (query instanceof $root.Base.HttpError), "QueryError IS a subclass of HttpError")
    test.ok(not (query instanceof $root.Base.InternalError), "QueryError IS a subclass of InternalError")

    test.ok(not (http instanceof $root.Base.QueryError), "HttpError IS a subclass of QueryError")
    test.ok(not (http instanceof $root.Base.InternalError), "HttpError IS a subclass of InternalError")

    test.ok(not (internal instanceof $root.Base.QueryError), "InternalError IS a subclass of QueryError")
    test.ok(not (internal instanceof $root.Base.HttpError), "InternalError IS a subclass of HttpError")


    test.done()

  "Test for catching http status errors":(test)->
    base = new $root.Base

    base.request {uri: "http://www.google.com/fun"},
    (error, response, body)->
      test.equal(error.code, $root.Base.HttpStatusError.name)
      test.equal(error.message,'Did a bad request to: www.google.com')
      test.done()

  "test timeout error":(test)->
    base = new $root.Base

    base.request {uri: "http://www.google.com:81", timeout: 10},
      (error, response, body)->
        test.equal(error.code, $root.Base.HttpTimeoutError.name)
        test.equal(error.message,'timeout')
        test.done()


  "Test each instance must have separated session":(test)->
    base1 = new $root.Base
    base2 = new $root.Base
    test.notEqual(base1.cookie_jar, base2.cookie_jar, "Fail in the creation of the instance")

    base1.request {uri: "http://www.google.com"},
    (error, response, body)->
      base2.request {uri: "http://www.google.com"},
      (error, response, body)->
        test.notDeepEqual(base1.cookie_jar, base2.cookie_jar, "Fail in the request library")
        test.done()


  "Test the default design guide that must follow all the crawlers":(test)->
    #list all the crawlers
    path = __dirname.replace("tests/",'')
    $root.fs.readdir path, (error,files)->
      test.equal error, null, "Bad path"
      for file in files
        if file == "base.coffee"
          continue

        names = []
        domains = []

        site = require $root.path.join(path, file)
        crawler = new site
        test.ok crawler.name?, file + " Should define a name"
        test.ok crawler.domain?, file + " Should define a domain"
        test.ok domains.indexOf(crawler.domain) == -1, file+" Should hava a unique domain"
        test.ok names.indexOf(crawler.name) == -1, file + " Should hava a unique name"
        names.push crawler.name
        domains.push crawler.domain
        base = new $root.Base
        #test implementation os site_search
        test.notEqual(crawler.search_hotel, base.search_hotel, "at last search_hotel should be implemented in "+ crawler.name)
        test.notEqual(crawler.translate_on_demand, base.translate_on_demand, "translate on demand must be implemented in "+ crawler.name )
        test.equal(crawler.search, base.search, crawler.name + " should not overide the search method" )
        test.equal(crawler.translate, base.translate, crawler.name + " should not overide the translate method" )

      test.done()

