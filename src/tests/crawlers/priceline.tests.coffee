PriceLine = require "../../crawlers/priceline"
cheerio = require "cheerio"
moment = require "moment"
redis = require "redis"

exports.PriceLineTestCases =

  "Test translate AlltheRooms params to priceline params":(test)->
    crawler = new PriceLine

    crawler_query =
      location: "Paris, France"
      start_date:'2013-06-24'
      end_date: '2013-06-27'
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    crawler.translate_on_demand crawler_query,
    (error, result)->
      test.ok result.jsk?, "jsk should be defined"
      test.ok result.plf?, "plf should be defined"
      test.ok result.location_translated =='3000035827', "location_id should be 3000035827"
      test.done()

  "search_hotel: Test when is enter a bad city":(test)->
    crawler = new PriceLine

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler_query =
      location: "OmicronPr6_8"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    """ call directly translate
    becouse this test is about how respond the site when exist
    a  bad input
    """
    crawler.translate crawler_query, (error, new_query)->
      test.ok(error instanceof PriceLine.CrawlerHttpError)
      test.ok(error.message=="Bad input city for priceline")
      test.done()

  "search_hotel: Test simple request":(test)->
    crawler = new PriceLine
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    client = redis.createClient(6379, '127.0.0.1')

    crawler_query =
      location: "Paris, France"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20
      redis: {host: "127.0.0.1", port: 6379}
      city_internal_id: "price_line_search_hotel_test"

    crawler.search {query:crawler_query},
    (error, result, paging)->
      test.equals(error, null)
      if not paging?
        #check city id
        client.get "price_line_search_hotel_test:priceline.com", (err, reply)->
          test.equals(reply, '3000035827')
          client.del "price_line_search_hotel_test:priceline.com", (err, reply)->
            client.end()
            test.done()

  "search_hotel: Test William Beckler design spec for priceline":(test)->
    crawler = new PriceLine

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler_query =
      location: "New York, NY"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    isNumber = (o)->
      typeof o == 'number' and isFinite(o);

    crawler.search {query:crawler_query},
    (error, reply, pagging)->

      for i in reply
        test.ok(isNumber(i.stars), "stars Should  be a number")
        test.ok(isNumber(i.price), "price Should  be a number")
        min_defined = if (i.neighborhood? and i.name? and i.stars? and i.price? and i.deeplink?) then true else false
        test.ok min_defined, "Should be defined at last neighborhood,stars, deeplink,name and price" + JSON.stringify(i)
        if i.rating?
          test.ok typeof i.rating is "string"
      if not pagging?
        return test.done()

  "search_express_deal: Test William Beckler design spec for priceline":(test)->
    crawler = new PriceLine

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler_query =
      search_type: "search_express_deal"
      location: "New York, NY"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    isNumber = (o)->
      typeof o == 'number' and isFinite(o);

    crawler.search {query:crawler_query, service:"priceline:search_express_deal"},
    (error, reply)->
      if error?
        console.error(error)
      test.equal error, null, "Should not have errors here"

      if reply?
        for i in reply
          test.ok(isNumber(i.stars), "stars Should  be a number")
          test.ok(isNumber(i.price), "price Should  be a number")
          min_defined = if i.neighborhood? and  i.stars? and i.price? and i.deeplink? then true else false
          test.ok min_defined, "Should be defined at last neighborhood,stars, deeplink and price"
          if i.rating?
            test.ok typeof i.rating is "string"
      test.done()

  "search_express_deal: Test when exist a error in the end_date on the query":(test)->
    crawler = new PriceLine

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    dummy = {}
    dummy.format = ()-> "sssasf"
    crawler_query =
      search_type: "search_express_deal"
      location: "Paris, France"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: dummy
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    """ call directly get_initial_params  and search_express_deal
    becouse this test is about how respond the site when exist
    a  bad input
    """

    crawler.search_express_deal crawler_query, (error, result)->
      test.ok(error instanceof PriceLine.CrawlerHttpError)
      test.ok(error.message.indexOf("Please enter a valid check-out date (mm/dd/yyyy)."))
      test.done()


  "search_express_deal: Test when exist a error in the start_date on the query":(test)->
    crawler = new PriceLine
    dummy = {}
    dummy.format = ()-> ""
    date2 = new Date(new Date().getTime() + 3 * 24 * 60 * 60 * 1000)
    crawler_query =
      search_type: "search_express_deal"
      location: "Paris, France"
      start_date: dummy
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    """ call directly get_initial_params  and search_express_deal
    becouse this test is about how respond the site when exist
    a  bad input
    """
    crawler.search_express_deal crawler_query, (error, result)->
      test.ok(error instanceof PriceLine.CrawlerHttpError)
      test.ok(error.message.indexOf("Please enter a valid check-in date (mm/dd/yyyy).")>=0)
      test.done()

  "search_express_deal: Test simple request":(test)->
    crawler = new PriceLine
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)
    client = redis.createClient(6379, '127.0.0.1')

    crawler_query =
      search_type: "search_express_deal"
      location: "London, UK"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20
      redis: {host: "127.0.0.1", port: 6379}
      city_internal_id: "price_line_search_express_test"

    crawler.search {query:crawler_query, service:"priceline:search_express_deal"},
    (error, result)->
      test.equals(error, null)
      client.get "price_line_search_express_test:priceline.com", (err, reply)->
        test.equals(reply, '3000035825')
        client.del "price_line_search_express_test:priceline.com", (err, reply)->
          client.end()
          test.done()

  "search_express_deal: Test when is enter a bad city":(test)->
    crawler = new PriceLine

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler_query =
      search_type: "search_express_deal"
      location: "OmicronPr6_8"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    crawler.search {query:crawler_query, service:"priceline:search_express_deal"},
    (error, response, body)->
      test.ok(error instanceof PriceLine.CrawlerHttpError)
      test.ok(error.message=="Bad input city for priceline")
      test.done()

  "search_express_deal: Test with bad number of rooms":(test)->
    crawler = new PriceLine

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler_query =
      search_type: "search_express_deal"
      location: "London, UK"
      start_date: moment(date).format('MM/DD/YYYY')  #use priceline format in this test
      end_date: moment(date2).format('MM/DD/YYYY')   #use priceline format in this test
      rooms: 'asd'
      guests: 2
      adults: 2
      children: 0
      timeout: 20000
    """ call directly get_initial_params  and search_express_deal
    becouse this test is about how respond the site when exist
    a  bad input
    """
    crawler.search_express_deal crawler_query, (error, result)->

      test.ok(error instanceof PriceLine.CrawlerHttpError)
      test.done()





