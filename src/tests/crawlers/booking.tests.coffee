Booking = require "../../crawlers/booking"
cheerio = require "cheerio"
moment = require "moment"
redis = require "redis"


exports.BookingTestCases =

  "search_hotel: Test William Beckler design spec for priceline":(test)->
    crawler = new Booking

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler_query =
      location: "New York, NY"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    isNumber = (o)->
      typeof o == 'number' and isFinite(o);

    crawler.search {query:crawler_query},
    (error, reply, pagging)->

      for i in reply
        test.ok(isNumber(i.stars), "stars Should  be a number")
        test.ok(isNumber(i.price), "price Should  be a number")
        min_defined = if (i.name? and i.stars? and i.price? and i.deeplink?) then true else false
        test.ok min_defined, "Should be defined at last neighborhood,stars, deeplink,name and price" + JSON.stringify(i)
        if i.rating?
          test.ok typeof i.rating is "string"
      if not pagging?
        return test.done()

  "search_hotel: Test simple request":(test)->
    crawler = new Booking
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    client = redis.createClient(6379, '127.0.0.1')
    crawler_query =
      location: "Paris, France"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 3
      guests: 0
      adults: 2
      children: 2
      timeout: 20
      redis: {host: "127.0.0.1", port: 6379}
      city_internal_id: "booking_search_hotel_test"

    crawler.search {query:crawler_query},(error, result, paging)->
      test.equals(error, null)
      if not paging?
        client.get "booking_search_hotel_test:booking.com", (err, reply)->
          test.equals(reply, '-1456928')
          client.del "booking_search_hotel_test:booking.com", (err, reply)->
            client.end()
            test.done()

  "Test translate AlltheRooms city to booking city":(test)->
    crawler = new Booking
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler_query =
      location: "Paris, France"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    crawler.translate_on_demand crawler_query,
    (error, result)->
      test.ok result.location_translated =='-1456928', "location_id should be -1456928"
      test.done()






a =
  "search_hotel: Test when is enter a bad city":(test)->
    crawler = new Booking

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler_query =
      location: "OmicronPr6_8"
      start_date: moment(date).format('YYYY-MM-DD')
      end_date: moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20000

    """ call directly translate
    becouse this test is about how respond the site when exist
    a  bad input
    """
    crawler.translate crawler_query, (error, new_query)->
      test.ok(error instanceof Booking.CrawlerHttpError)
      test.ok(error.message=="Bad input city for booking")
      test.done()

