$root = {}
$root.CrawlersTcp = require '../../servers/tcp'
$root.CrawlersProtocol = require '../../servers/crawlers_protocol'

$root.Base = require "../../crawlers/base"
$root.moment = require "moment"
$root.nano = require "nano"

exports.CrawlersProtocolTestCases =


  "Test saving errors in couchdb": (test)->

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    server = new $root.CrawlersTcp()
    client = new $root.CrawlersProtocol()
    errors = []
    nano = $root.nano("http://127.0.0.1:5984")
    errors_db = nano.use("errors")

    compare_errors = (error)->

      #compare error in the db with the error return in      crawl_search_error event
      errors.push(error)

      if errors.length == 2
        test.deepEqual(errors[0], errors[1], "Both errors (db adn socket) should be equals")
        server.socket.close()
        client.socket.end()
        client.socket.unref()
        test.done()

    client.on "crawl_search_error", (data)->
      compare_errors(data)

    client.on "couchdb_error_saved", (data)->
      errors_db.get data.id, (err, body, header)->
        delete  body._id
        delete body._rev
        delete body.date
        compare_errors(body)

    client.on "crawlers_discover", (data)->
      client.emit("change_dbs", {dbs:{couchdb:{host:"127.0.0.1",port:5984}}})
      crawler_query =
        location: "Wonderland"
        start_date: $root.moment(date).format('YYYY-MM-DD')
        end_date: $root.moment(date2).format('YYYY-MM-DD')
        rooms: 1
        guests: 2
        adults: 2
        children: 0
        timeout: 20

      data =
        hashkey: "TSS"
        services: ["hostwire:search_hotel"]
        query: crawler_query

      client.emit "crawl_search", data


    server.listen(2014,->
      client.connect('127.0.0.1',2014)
    )

  "Test basic socket functionalities": (test)->
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    server = new $root.CrawlersTcp()
    client = new $root.CrawlersProtocol()

    client.on "crawl_search_complete", (data)->
      #client.socket.close()
      server.socket.close()
      client.socket.end()
      client.socket.unref()
      test.equal(data.hashkey,"TSS")
      test.done()

    client.on "crawl_search_success", (data)->
      test.equal(data.hashkey,"TSS")


    client.on "crawlers_discover", (data)->
      test.deepEqual(data.services, server.services, "Should get the current active services")

      crawler_query =
        location: "Paris, France"
        start_date: $root.moment(date).format('YYYY-MM-DD')
        end_date: $root.moment(date2).format('YYYY-MM-DD')
        rooms: 1
        guests: 2
        adults: 2
        children: 0
        timeout: 20

      data =
        hashkey: "TSS"
        services: ["hostwire:search_hotel"]
        query: crawler_query
      client.emit "crawl_search", data


    server.listen(2013,->

      client.connect('127.0.0.1',2013)
    )


