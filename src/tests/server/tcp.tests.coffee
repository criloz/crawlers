$root = {}
$root.CrawlersTcp = require '../../servers/tcp'
$root.Base = require "../../crawlers/base"
$root.moment = require "moment"

exports.CrawlersTcpTestCases =
  "Test doing query with more of one servise using the same hashkey ": (test)->
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)
    services_used = {}
    server = new $root.CrawlersTcp()
    #mock emit function
    server.emit = (event, data)->
      if event=="crawl_search_complete"
        #search complete emited when both services end to crawl, this should happen once
        test.equal(data.stopped,false,"The search should end without stopping condition")
        test.ok(services_used["priceline:search_hotel"],"priceline:search_hotel should produce results")
        test.ok(services_used["hostwire:search_hotel"],"hostwire:search_hotel should produce results")
        test.done()
      else
        services_used[data.service] = true

    crawler_query =
      location: "Paris, France"
      start_date: $root.moment(date).format('YYYY-MM-DD')
      end_date: $root.moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    server.crawl_search("TSS", ["priceline:search_hotel", "hostwire:search_hotel"], crawler_query, 'uuid')

  "Test reliability of the server with bad inputs": (test)->

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    complete = []

    crawler_query =
      location: "Paris, France"
      start_date: $root.moment(date).format('YYYY-MM-DD')
      end_date: $root.moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    server = new $root.CrawlersTcp()
    complete = []
    #mock emit function
    server.emit = (event, data)->
      test.equal(event, "crawl_search_error", "All the emited events should be a error, the server should not fail ")
      test.ok(data.error?,"data should have the error object")
      complete.push(true)
      if complete.length == 9
        test.done()


    #bad hashkeys
    server.crawl_search([], ["priceline:search_hotel"], crawler_query, 'uuid')
    server.crawl_search(true, ["priceline:search_hotel"], crawler_query, 'uuid')
    server.crawl_search(null, ["priceline:search_hotel"], crawler_query, 'uuid')

    #bad services
    server.crawl_search("BS1", {wow:"pandarian"}, crawler_query, 'uuid')
    server.crawl_search("BS2", true, crawler_query,'uuid')
    server.crawl_search("BS3", null, crawler_query,'uuid')
    server.crawl_search("BS4", "warrior", crawler_query,'uuid')

    #bad query
    server.crawl_search("BQ1", ["priceline:search_hotel"], null, 'uuid')
    server.crawl_search("BQ2", ["priceline:search_hotel"], "warlock",'uuid')

  'Test dict clonation': (test)->
    base_dict =
      a: "b"
      c: "h"
      z: {w:"r"}
    server = new $root.CrawlersTcp()
    clon = server.clone(base_dict)
    test.notEqual(clon, base_dict, "Should not be the same object")
    test.deepEqual(clon, base_dict, "Should have the same content")
    test.done()

  'Test Stopping all the queries of a server': (test)->
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)
    complete = []
    stopped = false
    server = new $root.CrawlersTcp()
    #mock emit function
    server.emit = (event, data)->

      if event=="crawl_search_complete"
        complete.push(true)
        test.equal(data.stopped, true, "The search with TSS and XWW keys should end with stopping condition ")
        if complete.length==2
          test.done()
      else
        if not stopped
          server.stop(null, 'uuid')
          stopped = true

    crawler_query =
      location: "Paris, France"
      start_date: $root.moment(date).format('YYYY-MM-DD')
      end_date: $root.moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    server.crawl_search("TSS", ["priceline:search_hotel"], crawler_query, 'uuid')
    server.crawl_search("XWW", ["priceline:search_hotel"], crawler_query, 'uuid')

  'Test Stopping queries using hashkey ': (test)->
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)
    complete = []

    server = new $root.CrawlersTcp()
    #mock emit function
    server.emit = (event, data)->

      if event=="crawl_search_complete"
        complete.push(true)
        if data.hashkey == "TSS"
          test.equal(data.stopped, true, "The search with TSS key should end with stopping condition ")
        else
          test.equal(data.stopped,false,"The search should end without stopping condition")
        if complete.length==2
          test.done()
      else
        if data.hashkey == "TSS"
          server.stop("TSS")

    crawler_query =
      location: "Paris, France"
      start_date: $root.moment(date).format('YYYY-MM-DD')
      end_date: $root.moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    server.crawl_search("TSS", ["priceline:search_hotel"], crawler_query, 'uuid')
    server.crawl_search("XWW", ["priceline:search_hotel"], crawler_query, 'uuid')

  'Start a query then rempalce it with other query using the same hashkey ': (test)->
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    crawler_query =
      location: "Paris, France"
      start_date: $root.moment(date).format('YYYY-MM-DD')
      end_date: $root.moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    server = new $root.CrawlersTcp()
    complete = []
    success = []
    #mock emit function
    server.emit = (event, data)->
      if event=="crawl_search_complete"
        complete.push(true)
        if complete.length == 1
          test.equal(data.stopped, true, "The search should end with stopping condition due the replacement")
        else if complete.length == 2
          test.equal(data.stopped,false,"The search should end without stopping condition")
          test.done()
      else
        success.push(true)
        if complete.length == 0
          test.equal(data.service,"priceline:search_hotel","at last that the first search would be marked as complete,
 the result only should come from the first search")
        else if  complete.length == 1
          test.equal(data.service,"hostwire:search_hotel","if the first seach is marked as complete,  the result only
 should come from the second search")

        if success.length==2
          #overide search
          server.crawl_search("TSS", ["hostwire:search_hotel"], crawler_query, 'uuid')

    server.crawl_search("TSS", ["priceline:search_hotel"], crawler_query,'uuid')

  'Test parallel queries with diferent hashkeys': (test)->

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    server = new $root.CrawlersTcp()
    complete = []
    #mock emit function
    server.emit = (event, data)->

      if event=="crawl_search_complete"
        test.equal(data.stopped,false,"The search should end without stopping condition")
        complete.push(true)
        if complete.length == 2
          test.done()
      else
        if data.service == "priceline:search_hotel" then test.equal(data.hashkey, "TSS", "The hashkey must be equal to TSS when priceline:search_hotel")
        if data.service == "hostwire:search_hotel" then test.equal(data.hashkey, "XWW", "The hashkey must be equal to XSS when priceline:search_hotel")

    crawler_query =
      location: "Paris, France"
      start_date: $root.moment(date).format('YYYY-MM-DD')
      end_date: $root.moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    server.crawl_search("TSS", ["priceline:search_hotel"], crawler_query, 'uuid')
    server.crawl_search("XWW", ["hostwire:search_hotel"], crawler_query, 'uuid')

  'Test when there exist a error in the query': (test)->

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    server = new $root.CrawlersTcp()
    #mock emit function
    server.emit = (event, data)->
      if event=="crawl_search_complete"
        test.equal(data.stopped,false,"The search should end without stopping condition")
        test.done()
      else
        test.equal(data.service,"priceline:search_hotel", "The response should be generated by the service requested")
        test.equal(data.hashkey,"TSS")
        test.equal(event, "crawl_search_error", "Should emit  a error")

    crawler_query =
      #put bad city name
      location: "Cloud City"
      start_date: $root.moment(date).format('YYYY-MM-DD')
      end_date: $root.moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    server.crawl_search("TSS", ["priceline:search_hotel"], crawler_query, 'uuid')

  'Test search with a bad service name':(test)->

    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    server = new $root.CrawlersTcp()

    #mock emit function
    server.emit = (event, data)->
      if event=="crawl_search_complete"
        test.equal(data.stopped,false,"The search should end without stopping condition")
        test.done()
      else
        test.equal(event,"crawl_search_error", "Should have a error")
        test.equal(data.error.message,"The service @@:@@ not exist")

    crawler_query =
      location: "Paris, France"
      start_date: $root.moment(date).format('YYYY-MM-DD')
      end_date: $root.moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    server.crawl_search("TSS", ["@@:@@"], crawler_query)

  'Test simple search':(test)->
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    server = new $root.CrawlersTcp()
    #mock emit function
    server.emit = (event, data)->
      if event=="crawl_search_complete"
        test.equal(data.stopped,false,"The search should end without stopping condition")
        test.done()
      else
        test.equal(data.service,"priceline:search_hotel", "The response should be generated by the service requested")
        test.equal(data.hashkey,"TSS")

    crawler_query =
      location: "Paris, France"
      start_date: $root.moment(date).format('YYYY-MM-DD')
      end_date: $root.moment(date2).format('YYYY-MM-DD')
      rooms: 1
      guests: 2
      adults: 2
      children: 0
      timeout: 20

    server.crawl_search("TSS", ["priceline:search_hotel"], crawler_query, 'uuid')

  'Test the crawler discovery procedure':(test)->
    server = new $root.CrawlersTcp()
    [services,crawlers] = server.crawl_discover('../../crawlers')
    for service in Object.keys(crawlers)
      test.ok( new crawlers[service]() instanceof $root.Base, "all the crawlers should be subclass of base")
    test.done()

