/**
 * Created with JetBrains WebStorm.
 * User: cristian
 * Date: 1/06/13
 * Time: 05:10 AM
 * To change this template use File | Settings | File Templates.
 */

module =  angular.module('alltherooms', []);


module.filter('jsonPretty', function() {
    return function(input) {
        return JSON.stringify(input, null, '\t') ;
    };
});


module.factory('socket', function ($rootScope) {
    var socket = io.connect();
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }
    };
});

function appCtrl($scope){
    $scope.service_path = "/service" ;
    $scope.on_request = false;
    $scope.on_error = false;
    $scope.error_summary = {};
    $scope.sites = []
    $scope.success = {}
    $scope.results_on_success = {}
    $scope.results_on_success_counter = {}
    $scope.results_on_errors_counter = {}
    $scope.selectedIndex  = 0;
    $scope.paginationIndex  = {};
    $scope.change = function(index){
        $scope.selectedIndex = index;
        console.log($scope.selectedIndex);

    }



}

function formCtrl($scope, $http, socket){
    $scope.location =  'New York, NY';
    $scope.rooms =  1
    date = new Date(new Date().getTime() + 20 * 24 *60 *60 *1000)
    date2 = new Date(date.getTime() + 3 * 24 * 60 * 60 * 1000)

    $scope.start_date = date.getFullYear()+'-' + (date.getMonth()+1) + '-' + date.getDate()
    $scope.end_date =  date2.getFullYear()+'-' + (date2.getMonth()+1) + '-' + date2.getDate()
    $scope.guests = 2;
    $scope.adults = 2;
    $scope.children = 0;
    $scope.timeout = 20;
    $scope.services_on = {}
    socket.on("services", function(data) {
        $scope.$$$services = data
        for(i in  $scope.$$$services.services){
            $scope.services_on[$scope.$$$services.services[i]] = true
        }

    })

    socket.on("success", function(data){
        $scope.$parent.on_request = false;
        var site = data.site;
        delete data.site;

        //counter for success
        if ($scope.$parent.success[site]===null || $scope.$parent.success[site]===undefined ){
            $scope.$parent.success[site] = 0;
        }
        //store results as pages
        if ($scope.$parent.results_on_success[site]===null || $scope.$parent.results_on_success[site]===undefined ){
            $scope.$parent.results_on_success[site] = [];
        }
        //counter for results
        if ($scope.$parent.results_on_success_counter[site]===null || $scope.$parent.results_on_success_counter[site]===undefined ){
            $scope.$parent.results_on_success_counter[site] = 0;
        }
        // init counter for errors
        if ($scope.$parent.results_on_errors_counter[site]===null || $scope.$parent.results_on_errors_counter[site]===undefined ){
            $scope.$parent.results_on_errors_counter[site] = 0;
        }
        //init pagination
        if ($scope.$parent.paginationIndex[site]===null || $scope.$parent.paginationIndex[site]===undefined ){
            $scope.$parent.paginationIndex[site] = 0;
        }

        if ($scope.$parent.sites.indexOf(site)===-1){
            $scope.$parent.sites.push(site) ;

        }
        $scope.$parent.success[site] += 1;


        $scope.$parent.results_on_success[site].push(data);



        $scope.$parent.results_on_success_counter[site] += data.results.length


    })
    $scope.stop = function(){
        socket.emit("stop");

    }
    socket.on("error", function(data){

        $scope.$parent.on_request = false;
        var site = data.site;
        delete data.site;

        //counter for success
        if ($scope.$parent.success[site]===null || $scope.$parent.success[site]===undefined ){
            $scope.$parent.success[site] = 0;
        }
        //store results as pages
        if ($scope.$parent.results_on_success[site]===null || $scope.$parent.results_on_success[site]===undefined ){
            $scope.$parent.results_on_success[site] = [];
        }
        //counter for results
        if ($scope.$parent.results_on_success_counter[site]===null || $scope.$parent.results_on_success_counter[site]===undefined ){
            $scope.$parent.results_on_success_counter[site] = 0;
        }
        // init counter for errors
        if ($scope.$parent.results_on_errors_counter[site]===null || $scope.$parent.results_on_errors_counter[site]===undefined ){
            $scope.$parent.results_on_errors_counter[site] = 0;
        }
        //init pagination
        if ($scope.$parent.paginationIndex[site]===null || $scope.$parent.paginationIndex[site]===undefined ){
            $scope.$parent.paginationIndex[site] = 0;
        }

        if ($scope.$parent.sites.indexOf(site)===-1){
            $scope.$parent.sites.push(site) ;

        }
        $scope.$parent.results_on_errors_counter[site] += 1;


        $scope.$parent.results_on_success[site].push({"results":data});
        var key;
        var keys;
        var index;
        var summary = {} ;
        var ignore = ["status","body","response_header","options","status_code"];
        for(index in (keys =Object.keys(data))){
            key = keys[index];
            if(ignore.indexOf(key)>-1){
                continue;
            }
            else{
                summary[key] = data[key];

            }
        }
        summary.site = site;
        $scope.$parent.error_summary[site] = summary;


    } )

    $scope.search = function(){
        var key;
        var keys;
        var index;
        var form = {}
        var serialize = function(obj) {
            var str = [];
            for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            return str.join("&");
        }
        for(index in (keys =Object.keys($scope))){
           key = keys[index];
           if(key.indexOf("$")==0){
               continue;
           }
           else if(key==="search" || key==="this"){
               continue;
           }
           else{
               form[key] = $scope[key];

           }
        }
        socket.emit('search', form);
        $scope.$parent.on_request = true;


        $scope.$parent.on_error = false;
        $scope.$parent.error_summary = {};
        $scope.$parent.sites = []
        $scope.$parent.success = {}
        $scope.$parent.results_on_success = {}
        $scope.$parent.results_on_success_counter = {}
        $scope.$parent.results_on_errors_counter = {}
        $scope.$parent.selectedIndex  = 0;
        $scope.$parent.paginationIndex  = {};

        //var url = $scope.$parent.service_path +"/?" +serialize(form)
        //$scope.$parent.on_request = true;
        //$scope.$parent.on_error = false;

        /*
        $http.get(url).success(function(data, status, headers,  config ){
            $scope.$parent.reply = data;
            $scope.$parent.on_request = false;
            if(data.status=="error"){
                $scope.$parent.on_error = true;
                $scope.$parent.error_summary = {};
                var key;
                var keys;
                var index;
                var ignore = ["status","body","response_header","options","status_code"];
                for(index in (keys =Object.keys(data))){
                    key = keys[index];
                    if(ignore.indexOf(key)>-1){
                        continue;
                    }
                    else{
                        $scope.$parent.error_summary[key] = data[key];

                    }
                }
                console.log($scope.$parent.error_summary);


            }
        })   */



    }

}