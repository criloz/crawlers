grunt = require('grunt')

grunt.loadNpmTasks('grunt-contrib-nodeunit');


grunt.initConfig({

    nodeunit: {
        all: ['src/tests/**/*.tests.coffee']
    }

})